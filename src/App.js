import 'assets/css/style.css';
import 'assets/css/custom.css';
import { Route, Routes, Navigate } from 'react-router-dom';
import Homepage from './page/Homepage';
import Shop from 'page/Shop';
import Login from 'page/Login';
import Profile from 'page/Profile';
import ProductInfo from 'page/ProductInfo';

const RequireAuth = ({ children }) => {
    const isLoggedIn = window.localStorage.getItem("isLoggedIn");
    return isLoggedIn ? children : <Navigate to="/login" />
};
const App = () => {
        return (
            <Routes>
                <Route path='/' element={<Homepage />} />
                <Route path='/shop?category/:id' element={<Shop />} />
                <Route path='/shop' element={<Shop />} />
                <Route path='/shop/:id' element={<ProductInfo />} />
                <Route path='/login' element={<Login />} />
                <Route path='/profile' element={
                        <RequireAuth>
                            <Profile />
                        </RequireAuth>
                    } />
            </Routes>
    );
}
export default App;
