import 'assets/css/style.css';
import 'assets/css/custom.css';
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import BannerAllProduct from 'component/BannerAllProduct';
import AllProduct from 'component/AllProduct';
import Navbar from 'component/Navbar';
import Loading from 'component/Loading';
import { useSearchParams } from 'react-router-dom'
import Footer from 'component/Footer';

const Shop = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [allProduct, setAllProduct] = useState([]);
    const [catProduct, setCatProduct] = useState([]);
    const [catName, setCatName] = useState('');


    const [param] = useSearchParams()
    const category = param.get('category')


    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/products`)
            .then(function (response) {
                const { data } = response;
                setAllProduct(data.data.productList);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])
    console.log("All product", allProduct)

    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/products?category=${category}`)
            .then(function (response) {
                const { data } = response;
                setCatProduct(data.data.productList);
                setCatName(data.data.categoryDetail.name);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [category])
    console.log("data by category id", category)


    return (
        <>
            <Navbar /><div className="content padding" style={{ maxWidth: '1564px' }}>
                {isLoading ? <Loading /> :
                    <>
                        {category == null ?
                            <BannerAllProduct /> :
                            <div className="container padding-32" id="about">
                                <h3 className="border-bottom border-light-grey padding-16">
                                    Product Category {catName}
                                </h3>
                            </div>}

                        <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                            {category == null ?
                                allProduct?.map(item => {
                                    return <AllProduct key={item.id} item={item} />;
                                }) : catProduct?.map(item => {
                                    return <AllProduct key={item.id} item={item} />;
                                })}
                        </div>
                    </>}
            </div>
            <Footer />
        </>
    )
}

export default Shop