import 'assets/css/style.css';
import 'assets/css/custom.css';
import Navbar from 'component/Navbar';
import Header from 'component/Header';
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import ProductCategory from 'component/ProductCategory';
import BannerProductCategory from 'component/BannerProductCategory';
import Loading from 'component/Loading';
import Footer from 'component/Footer';
import Map from 'component/Map';

const Homepage = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [productCat, setProductCat] = useState([]);


    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/categories`)
            .then(function (response) {
                const { data } = response;
                setProductCat(data.data);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])
    console.log("product categoty", setProductCat)

    return (
        <>
            <Navbar />
            <Header />
            {
                isLoading ? <Loading /> :
                    <>
                    <div className="content padding" style={{ maxWidth: '1564px' }}>
                        <BannerProductCategory />
                        <div className="row-padding">
                            {
                                productCat?.map(item => {
                                    return <ProductCategory key={item.id} item={item} />
                                })
                            }
                        </div>
                        </div>
                    </>
            }
            <div className="content padding" style={{ maxWidth: '1564px' }}><Map /></div>
            <Footer />
        </>
    );
}

export default Homepage