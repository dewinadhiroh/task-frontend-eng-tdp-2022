import axios from 'axios';
import React, { useEffect, useState } from 'react'
import Navbar from 'component/Navbar';

const Profile = () => {
    const [profileData, setProfile] = useState({
        name: "",
        email: "",
        phoneNumber: ""
    })

    const [password, setPassword] = useState({
        password: "",
        retypePassword: ""
    })
//for validation
    const [notif, setNotif] = useState({
        field: '',
        messageContent: ''
    });

    //get data profile from local storage, var will invoke in getProfile method
    const username = window.localStorage.getItem("username");

    const getProfile = async () =>{
    // Error handling pada async/await menggunakan try…catch
        try{
            let result = await axios.get(`https://api-tdp-2022.vercel.app/api/profile/${username}`);
            setProfile(result.data.data);
            }
        catch(e){
        }
    }
//mounting session
    useEffect(()=>{
        getProfile();
    }, [])

    const handleChange = (e) => {
        let data = {...profileData};
        data[e.target.name] = e.target.value;
        setProfile(data)

        let pass = {...password};
        pass[e.target.name] = e.target.value;
        setPassword(pass)
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        //Destructuring Assigment - Array
        let dataFill = {...profileData};
        
        if(dataFill.name === ""){
            setNotif({
                field: 'name',
                messageContent: "Name is required"
            })
            return false

        }if(dataFill.email === ""){
            setNotif({
                field: 'email',
                messageContent: "Email is required"
            })
            return false

        }if(dataFill.phoneNumber === ""){
            setNotif({
                field: 'phoneNumber',
                messageContent: "Phone number is required"
            })
            return false

        }if(password.password === ""){
            setNotif({
                field: 'password',
                messageContent: "Password is required"
            })
            return false

        }if(password.retypePassword === ""){
            setNotif({
                field: 'retypePassword',
                messageContent: "Retype Password is required"
            })
            return false
            
        }if(password.password !== password.retypePassword){
            setNotif({
                field: 'retypePassword',
                messageContent: "Retype Password invalid"
            })
            return false
        }if (dataFill.name === "" || dataFill.email === "" || dataFill.phoneNumber === "" || password.password === "" || password.retypePassword === "") {
            notif(<div style={{ color: '#EF144A' }}>{notif.messageContent}</div>)
            return false;
          }else{
            const updateData = {
                name: profileData.name,
                email: profileData.email,
                phoneNumber: profileData.phoneNumber,
                password: password.password
            }
            axios.put( `https://api-tdp-2022.vercel.app/api/profile/${username}`, updateData)
            .then(result => {
            if(result){
                // console.log(result?.data?.message)
                alert(result?.data?.message)
                window.location.replace("/");
            }
            })
        }
        return true
    }
    console.log('username', username)
    console.log('getData profile by username', profileData)

  return (
    <div className="content padding" style={{ maxWidth: '1564px' }}>
        <Navbar />
        <div className="container padding-32" id="contact" >
            <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
            <p>Lets get in touch and talk about your next project.</p>
            <form onSubmit={handleSubmit}>
                <input className="input section border" type="text" placeholder="Name" onChange={handleChange} value={profileData.name} name="name" />
                {
                    (notif.field==="name")?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <input className="input section border" type="email" placeholder="Email" onChange={handleChange} value={profileData.email} name="email" />
                {
                    (notif.field==="email")?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <input className="input section border" type="text" placeholder="Phone Number" onChange={handleChange} value={profileData.phoneNumber} name="phoneNumber" maxLength={12} />
                {
                    (notif.field==="phoneNumber")?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <input className="input section border" type="password" placeholder="Password" name="password" onChange={handleChange} value={password.password} />
                {
                    (notif.field==="password")?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <input className="input section border" type="password" placeholder="Retype Password" name="retypePassword" onChange={handleChange} value={password.retypePassword} />
                {
                    (notif.field==="retypePassword")?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <button className="button black section" type="submit">
                    <i className="fa fa-paper-plane" /> Update
                </button>
            </form>
        </div>
    </div>
  )
}

export default Profile