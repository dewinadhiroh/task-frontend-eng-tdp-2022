import React, { Fragment, useState } from 'react'
import axios from 'axios';
import Navbar from 'component/Navbar';

const Login = () => {

    const [username, setUserName] = useState();
    const [password, setPassword] = useState();


    const handleSubmit = async e => {
        e.preventDefault();
        
        const data = {
            username: username,
            password: password
        }
        axios.post(`https://api-tdp-2022.vercel.app/api/login`, data)
            .then(resolve => {
                    window.localStorage.setItem('token', resolve.data.token);
                    window.localStorage.setItem("isLoggedIn", true);
                    window.localStorage.setItem("username", username);
                    window.location.href = "/";
                }).catch(function (error) {
                    alert(error?.response?.data?.message);
                });    
    }
    return (
        <>
        <Navbar />
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="contact" >
                    <h3 className="border-bottom border-light-grey padding-16">Login</h3>
                    <p>Lets get in touch and talk about your next project.</p>
                    <form onSubmit={handleSubmit}>
                        <input className="input border" type="text" onChange={e => setUserName(e.target.value)} placeholder="Username" required name="username" />
                        <input className="input section border" onChange={e => setPassword(e.target.value)} type="password" placeholder="Password" required name="Password" />
                        <button className="button black section" type="submit" >
                            <i className="fa fa-paper-plane" />
                            Login
                        </button>
                    </form>
                </div >
            </div>
        </>
    )
}
export default Login