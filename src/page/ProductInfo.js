import Navbar from 'component/Navbar'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import ProductData from 'component/ProductData'
import Footer from 'component/Footer'



const ProductInfo = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [productInfo, setProductInfo] = useState({});
    let params = useParams();


    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/products/${params.id}`)
            .then(function (response) {
                const { data } = response;
                setProductInfo(data.data);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [params.id])
    console.log("product information detail", productInfo)

    return (
        <><div className="content padding" style={{ maxWidth: '1564px' }}>
            <Navbar />
            {isLoading ? <div>Loading...</div> :
                <ProductData key={productInfo.id} item={productInfo} />})
        </div><Footer /></>


    )
}

export default ProductInfo