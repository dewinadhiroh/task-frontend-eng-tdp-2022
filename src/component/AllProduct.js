import React from 'react'

const AllProduct = ({ item }) => {
    //The toFixed() method converts a number to a string.The toFixed() method rounds the string to a specified number of decimals.
    function currencyFormat(num) {
        return 'Rp ' + num.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
    
    let dataPrice = parseInt(item.price)
    let price = currencyFormat(dataPrice)

    return (
        <div className="product-card">
            {item.discount > 0 ? <div className="badge">Discount</div> : ""}
            <div className="product-tumb">
                <img src={item.image} alt="product1" />
            </div>
            <div className="product-details">
                <span className="product-catagory">{item.category}</span>
                <h4>
                    <a href={`/shop/1`}>{item.title}</a>
                </h4>
                <p>{item.description}</p>
                <div className="product-bottom-details">
                    <div className="product-price">{price}</div>
                    <div className="product-links">
                        <a href={`/shop/${item.id}`}>View Detail<i className="fa fa-heart"></i></a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AllProduct