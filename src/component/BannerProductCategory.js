import React from 'react'

const BannerProductCategory = () => {
    return (
        <div className="container padding-32" id="projects">
            <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
        </div>
    )
}

export default BannerProductCategory