import { useParams } from 'react-router-dom'
import React, { useState } from 'react'

const ProductData = ({ item }) => {
    let params = useParams();
    const [dataQuantity, setDataQuantity] = useState('')
    // const isLoggedIn = window.localStorage.getItem("isLoggedIn");

    function cart() {
        alert('Product added to cart successfully')
        window.location.replace("/");
    }

    function currencyFormat(num) {
        return 'Rp ' + num.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
    let dataPrice = parseInt(item.price)
    let diskon = 1 - (item?.discount / 100);
    let newPrice = currencyFormat(dataPrice * dataQuantity);
    let subTotal = currencyFormat(dataPrice * dataQuantity * diskon)
    let price = currencyFormat(dataPrice)

    return (
        <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
            <div className="col l3 m6 margin-bottom">
                <div className="product-tumb">
                    <img src={item?.image} alt="Product" {...params.id} />
                </div>
            </div>
            <div className="col m6 margin-bottom">
                <h3>{item?.title}</h3>
                <div style={{ marginBottom: '32px' }}>
                    <span>Category : <strong>{item?.category}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Review : <strong>{item?.rate}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Stock : <strong>{item?.stock}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Discount : <strong>{item?.discount} %</strong></span>
                </div>
                <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                    {price}
                </div>
                <div style={{ marginBottom: '32px' }}>
                    {item?.description}
                </div>
                <div style={{ marginBottom: '32px' }}>
                    <div><strong>Quantity : </strong></div>
                    <input type="number" onChange={event => setDataQuantity(event.target.value)} min={0} max={item?.stock} className="input section border" name="total" defaultValue={'0'} />

                </div>
                <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                    Sub Total : {subTotal}
                    {
                        item.discount && item.discount ?
                            <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>{newPrice}</strong></span> : ''
                    }
                </div>
                {
                    item.stock > 0 ?
                    //dataQuantity ===0
                        dataQuantity === 0 || dataQuantity === '' || dataQuantity === '0'?
                            <button className='button light-grey block' disabled={true}>Add to cart</button>
                            : <button className='button light-grey block' onClick={cart}>Add to cart</button>
                        : <button className='button light-grey block' disabled={true}>Empty Stock</button>
                }
            </div>
        </div>
    )
}

export default ProductData